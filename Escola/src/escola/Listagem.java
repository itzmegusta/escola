/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escola;

import escola.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

/**
 *
 * @author Gustavo
 */
public class Listagem {
    public DefaultListModel listaAlunos(){
        
        DefaultListModel lista = new DefaultListModel();
         try {
            Conexao c= new Conexao();
            Connection con=c.obterConexao();
            String strSQL="select * from sc_aula.tb_aluno";
            Statement s=con.createStatement();
            ResultSet r=s.executeQuery(strSQL);
            while(r.next()){
                /*System.out.println("Regiao   Nome_doença");
                System.out.println(r.getString(1)+"  "+r.getString(2));*/
                if(lista.contains(r.getString(1) + "   /   " + r.getString(2) + "   /   " + r.getString(3) + "   /   " + r.getString(4))){
                    return null;
                }
                lista.addElement(r.getString(1) + "           /          " + r.getString(2) + "          /             " + r.getString(3) + "            /          " + r.getString(4));
            }
            r.close();
            con.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Alunos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     public DefaultListModel listaProfessores(){
        
        DefaultListModel lista = new DefaultListModel();
         try {
            Conexao c= new Conexao();
            Connection con=c.obterConexao();
            String strSQL="select * from sc_aula.tb_professor";
            Statement s=con.createStatement();
            ResultSet r=s.executeQuery(strSQL);
            while(r.next()){
                /*System.out.println("Regiao   Nome_doença");
                System.out.println(r.getString(1)+"  "+r.getString(2));*/
                if(lista.contains(r.getString(1) + "   /   " + r.getString(2) + "   /   " + r.getString(3) + "   /   " + r.getString(4))){
                    return null;
                }
                lista.addElement(r.getString(1) + "           /          " + r.getString(2) + "          /             " + r.getString(3) + "            /          " + r.getString(4));
            }
            r.close();
            con.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Alunos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}